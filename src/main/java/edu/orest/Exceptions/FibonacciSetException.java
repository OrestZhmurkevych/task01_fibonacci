package edu.orest.Exceptions;

public class FibonacciSetException extends IllegalArgumentException{
    public FibonacciSetException(String message) {
        super(message);
    }
}
