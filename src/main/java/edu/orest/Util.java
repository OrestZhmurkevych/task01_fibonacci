package edu.orest;

/**
 * Service class for application.
 */
public class Util {

    public static final int ONE_HUNDRED = 100;

    /**
     * Gets an interval between two numbers.
     *
     * @param start start of interval
     * @param end   end of interval
     * @return counted interval.
     */
    public static int[] getInterval(final int start, final  int end) {
        int[] result = new int[end - start + 1];
        result[0] = start;
        for (int i = 1; i < result.length; i++) {
            result[i] = result[i - 1] + 1;
        }
        return result;
    }

    /**
     * Prints odd numbers from the given array of integers.
     * @param array array of integers.
     */
    public static void printOddNumbersFromArray(final int[] array) {
        System.out.println("\nOdd numbers from array:");
        for (int item : array) {
            if (item % 2 == 1 && item % 2 == -1) {
                System.out.print(item + " ");
            }
        }
        System.out.println("\n");
    }

    /**
     * Prints even numbers from the given array of integers.
     * @param array array of integers.
     */
    public static void printEvenNumbersFromArray(final int[] array) {
        System.out.println("\nEven numbers from array:");
        for (int item : array) {
            if (item % 2 == 0) {
                System.out.print(item + " ");
            }
        }
        System.out.println("\n");
    }

    /**
     * Adds all even numbers from the given array of integers and
     * returns the general sum of these numbers.
     * @param array array of integers
     * @return general sum of even numbers from the given array of integers.
     */
    public static int sumEvenNumbers(final int[] array) {
        int result = 0;
        for (int item : array) {
            if (item % 2 == 0) {
                result += item;
            }
        }
        return result;
    }

    /**
     * Adds all odd numbers from the given array of integers and
     * returns the general sum of these numbers.
     * @param array array of integers
     * @return general sum of odd numbers from the given array of integers.
     */
    public static int sumOddNumbers(final int[] array) {
        int result = 0;
        for (int item : array) {
            if (item % 2 == 1 && item % 2 == -1) {
                result += item;
            }
        }
        return result;
    }

    /**
     * Defines the biggest odd number in the
     * given array of integers and returns it.
     * @param array array of integers
     * @return the biggest odd number from the given array of integers.
     */
    public static int getMaxOddNumber(final int[] array) {
        int result = array[0];
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 == 1 && array[i] % 2 == -1 && result < array[i]) {
                result = array[i];
            }
        }
        return result;
    }

    /**
     * Defines the biggest even number in the
     * given array of integers and returns it.
     * @param array array of integers
     * @return the biggest even number from the given array of integers.
     */
    public static int getMaxEvenNumber(final int[] array) {
        int result = array[0];
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 == 0 && result < array[i]) {
                result = array[i];
            }
        }
        return result;
    }

    /**
     * Builds the fibonacci set and returns it.
     * @param firstNumber first number of the fibonacci set
     * @param secondNumber second number of the fibonacci set
     * @param size the amount of numbers in the fibonacci set
     * @return the fibonacci set in the array of integers.
     */
    public static int[] getFibonacciSet(final int firstNumber,
                                       final int secondNumber,final int size) {
        int[] result = new int[size];
        result[0] = firstNumber;
        result[1] = secondNumber;
        for (int i = 2; i < size; i++) {
            result[i] = result[i - 1] + result[i - 2];
        }
        return result;
//        getFibonacciSet(secondNumber,nextElement,size);
    }

    /**
     * Prints the given array of integers on console.
     * @param array array of integers.
     */
    public static void printArray(final int[] array) {
        System.out.println("\nThe array is: ");
        for (int item : array) {
            System.out.print(item + " ");
        }
        System.out.println("\n");
    }

    /**
     * Counts the amount of odd numbers in the given array of integers.
     * @param array array of integers
     * @return the amount of odd numbers.
     */
    public static int countOfOddNumbers(final int[] array) {
        int result = 0;
        for (int item : array) {
            if (Math.abs(item) % 2 == 1) {
                result++;
            }
        }
        return result;
    }

    /**
     * Counts the amount of even numbers in the given array of integers.
     * @param array array of integers
     * @return the amount of even numbers.
     */
    public static int countOfEvenNumbers(final int[] array) {
        int result = 0;
        for (int item : array) {
            if (item % 2 == 0) {
                result++;
            }
        }
        return result;
    }

    /**
     * Counts the percentage of one number from another one.
     * @param generalAmount the overall number
     * @param amount the number for which the method counts
     *               the percentage from overall number
     * @return the percentage.
     */
    public static String percentage(final int generalAmount, final int amount) {
        double result = amount * ONE_HUNDRED / generalAmount;
        return String.format("%.2f", result);
    }
}
