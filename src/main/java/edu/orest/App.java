package edu.orest;


import edu.orest.Exceptions.FibonacciSetException;
import edu.orest.Resources.ResourceForFibonacciNumbers;

import java.util.Scanner;

/**
 * Application class which contains entry point of application.
 */
public class App {


    /**
     * Main method(entry point of application).
     *
     * @param args
     */
    public static void main(final String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter the start of the interval --> ");
        final int startInterval = scan.nextInt();
        System.out.print("Enter the end of the interval --> ");
        final int endInterval = scan.nextInt();
        final int[] interval = Util.getInterval(startInterval, endInterval);
        Util.printOddNumbersFromArray(interval);
        Util.printEvenNumbersFromArray(interval);
        System.out.println("\nThe sum of odd numbers is: ");
        System.out.println(Util.sumOddNumbers(interval));
        System.out.println("\nThe sum of even numbers is: ");
        System.out.println(Util.sumEvenNumbers(interval));
        System.out.println("\nThe biggest odd and even numbers are: ");
        System.out.print(Util.getMaxOddNumber(interval) + " " +
                Util.getMaxEvenNumber(interval));
        try (ResourceForFibonacciNumbers resource = new ResourceForFibonacciNumbers()){
            System.out.print("\nEnter the size of Fibonacci set --> ");
            final int sizeOfFibonacciSet = scan.nextInt();
            if(sizeOfFibonacciSet <= 0 ){
                throw new FibonacciSetException("Fibonacci set exception occurred");
            }
            final int[] fibonacciSet = Util
                    .getFibonacciSet(Util.getMaxOddNumber(interval),
                            Util.getMaxEvenNumber(interval), sizeOfFibonacciSet);
            Util.printArray(fibonacciSet);
            System.out.println
                    ("The percentage of odd numbers in fibonacci set is: ");
            System.out.println(Util.percentage(sizeOfFibonacciSet,
                    Util.countOfOddNumbers(fibonacciSet)));
            System.out.println
                    ("\nThe percentage of even numbers in fibonacci set is: ");
            System.out.println(Util.percentage(sizeOfFibonacciSet,
                    Util.countOfEvenNumbers(fibonacciSet)));
        }
        catch (FibonacciSetException fibSetEx){
            System.out.println("First catch block invoked");
            System.out.println(fibSetEx.getMessage());
        }
        catch (Exception genEx){
            System.out.println("Second catch block invoked");
            System.out.println(genEx.getMessage());
        }
    }
}
